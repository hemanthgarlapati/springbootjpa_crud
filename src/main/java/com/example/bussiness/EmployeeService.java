package com.example.bussiness;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.DTO.EmployeeExpDTO;
import com.example.entity.Employee;
import com.example.repository.EmployeeRepository;

@Service
public class EmployeeService {

	@Autowired
	EmployeeRepository employeeRepository;

	public List<Employee> getAllEmployees() {
		return (List<Employee>) employeeRepository.findAll();
	}

	public Employee findById(Integer id) {
		return employeeRepository.findById(id).get();
	}

	public void deleteEmployee(Integer id) {
		employeeRepository.deleteById(id);
	}

	public boolean addEmployee(Employee employee) {
		return employeeRepository.save(employee) != null;
	}

	public List<Employee> getEmpByExperience(Integer experience) {
		return employeeRepository.findByExperience(experience);
	}

	public List<EmployeeExpDTO> groupEmpByExperience() {
		return employeeRepository.groupEmpByExperience();
	}
	
	public List<Map<String,Object>> groupEmpByExperienceWithNative() {
		return employeeRepository.groupEmpByExperienceWithNative();
	}
	
	public List<Employee>  findByExperienceLessThan(Integer experience) {
		return employeeRepository.findByExperienceLessThan(experience);
	}
	
	public List<Employee>  findByExperienceGreaterThan(Integer experience) {
		return employeeRepository.findByExperienceGreaterThan(experience);
	}
	
	public List<Employee>  findByExperienceBetween(Integer start, Integer end) {
		return employeeRepository.findByExperienceBetween(start,end);
	}
	
	public Page<Employee> findAllWithPagination(Integer pageNumber, Integer pageSize) {
		Pageable pageable = PageRequest.of(pageNumber, pageSize);
		return employeeRepository.findAll(pageable);
	}


	public List<Employee> findByExperience(Integer pageNumber, Integer pageSize, Integer experience, String field) {
		Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(field));
		return employeeRepository.findByExperience(experience, pageable);
	}

}
