package com.example.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.DTO.EmployeeExpDTO;
import com.example.bussiness.EmployeeService;
import com.example.entity.Employee;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

	@Autowired
	EmployeeService employeeService;

	@ApiOperation("Retrieve all the employees")
	@GetMapping(value = "")
	public ResponseEntity<List<Employee>> getAllEmployees() throws Exception {

		List<Employee> employees = employeeService.getAllEmployees();

		if (!employees.isEmpty()) {
			return ResponseEntity.ok().body(employees);
		} else {
			throw new Exception("Employees not found");
		}
	}
	
	@ApiOperation("Retrieves the EmployeeInfo based on ID")
	@GetMapping(value = "/{id}")
	public ResponseEntity<Employee> getEmployeeById(@PathVariable Integer id) {

		Employee employee = employeeService.findById(id);

		return ResponseEntity.ok().body(employee);

	}
	
	@ApiOperation("Inserts new Employee into DB")
	@PostMapping(value = "")
	public ResponseEntity<String> insertEmployee(@RequestBody Employee employee) {

		if (employeeService.addEmployee(employee))
			return new ResponseEntity<>("Employee with ID: " + employee.getId() + " is inserted successfully",
					HttpStatus.CREATED);
		else
			return new ResponseEntity<>("some error occured", HttpStatus.INTERNAL_SERVER_ERROR);

	}
	
	@ApiOperation("Updates existing Employee")
	@PutMapping(value = "/{id}")
	public ResponseEntity<String> updateEmployee(@RequestBody Employee employee, @PathVariable Integer id) {

		if (employeeService.addEmployee(employee))
			return new ResponseEntity<>("Employee with ID: " + employee.getId() + " is updated successfully",
					HttpStatus.OK);
		else
			return new ResponseEntity<>("some error occured", HttpStatus.INTERNAL_SERVER_ERROR);

	}
	
	@ApiOperation("Deletes employee from DB based on ID")
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<String> deleteEmployee(@PathVariable Integer id) {

		try {
			employeeService.deleteEmployee(id);
			return new ResponseEntity<>("Employee with ID: " + id + " is deleted successfully", HttpStatus.OK);

		} catch (Exception exception) {
			return new ResponseEntity<>("some error occured", HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	@ApiOperation("Retrieves all the employees with particular experience")
	@GetMapping(value = "/experience/{experience}")
	public ResponseEntity<List<Employee>> getEmpByExperience(@PathVariable Integer experience) {
		return ResponseEntity.ok().body(employeeService.getEmpByExperience(experience));
	}
	
	@ApiOperation("Groups the employees by experience")
	@GetMapping(value = "/experience")
	public ResponseEntity<List<EmployeeExpDTO>> groupEmpByExperience() {
		return ResponseEntity.ok().body(employeeService.groupEmpByExperience());
	}

	@ApiOperation("Groups the employees by experience using Native query")
	@GetMapping(value = "/experienceNative")
	public ResponseEntity<List<Map<String, Object>>> groupEmpByExperienceWithNative() {
		return ResponseEntity.ok().body(employeeService.groupEmpByExperienceWithNative());
	}
	
	@ApiOperation("Retrieves employees with  experience less than particular value")
	@GetMapping(value = "/experienceLesser/{experience}")
	public ResponseEntity<List<Employee>> findByExperienceLessThan(@PathVariable Integer experience) {
		return ResponseEntity.ok().body(employeeService.findByExperienceLessThan(experience));
	}
	
	@ApiOperation("Retrieves employees with  experience greater than particular value")
	@GetMapping(value = "/experienceGreater/{experience}")
	public ResponseEntity<List<Employee>> findByExperienceGreaterThan(@PathVariable Integer experience) {
		return ResponseEntity.ok().body(employeeService.findByExperienceGreaterThan(experience));
	}
	
	@ApiOperation("Retrieves employees with  experience in b/w 2 values")
	@GetMapping(value = "/experienceBetween/{start}/{end}")
	public ResponseEntity<List<Employee>> findByExperienceBetween(@PathVariable Integer start,@PathVariable Integer end) {
		return ResponseEntity.ok().body(employeeService.findByExperienceBetween(start,end));
	}
	
	@ApiOperation("Retrives all the employees with paging")
	@GetMapping(value = "/{pageNo}/{pageSize}")
	public ResponseEntity<List<Employee>> findAllWithPagination(@PathVariable Integer pageNo, @PathVariable Integer pageSize) {
		return ResponseEntity.ok().body(employeeService.findAllWithPagination(pageNo, pageSize).getContent());
	}
	 
	@ApiOperation("Retrives all the employees with particular experience with paging and sorting")
	@GetMapping(value = "/{experience}/{field}/{pageNo}/{pageSize}")
	public ResponseEntity<List<Employee>> pageAndSortEmp(@PathVariable Integer experience, @PathVariable String field,
			@PathVariable Integer pageNo, @PathVariable Integer pageSize) {
		return ResponseEntity.ok().body(employeeService.findByExperience(pageNo, pageSize, experience, field));
	}

}
