package com.example.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.DTO.EmployeeExpDTO;
import com.example.entity.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

	public List<Employee> findByExperience(Integer experience);
	
	Page<Employee> findAll(Pageable pageable);

	@Query("SELECT new com.example.DTO.EmployeeExpDTO(e.experience, COUNT(e.experience)) "
			+ "FROM Employee AS e GROUP BY e.experience")
	public List<EmployeeExpDTO> groupEmpByExperience();
	
	@Query(value="SELECT E.EXPERIENCE , COUNT(E.EXPERIENCE) AS COUNT FROM EMPLOYEE E GROUP BY E.EXPERIENCE",nativeQuery = true)
	public List<Map<String,Object>> groupEmpByExperienceWithNative();

	public List<Employee> findByExperience(Integer experience, Pageable pageable);
	
	@Query(value="FROM Employee e WHERE e.experience < ?1")
	public List<Employee> findByExperienceLessThan(Integer experience);
	
	@Query(value="FROM Employee e WHERE e.experience > ?1")
	public List<Employee> findByExperienceGreaterThan(Integer experience);
	
	@Query(value="FROM Employee e WHERE e.experience > ?1 and e.experience < ?2")
	public List<Employee> findByExperienceBetween(Integer start, Integer end);
}
